# Implementation:

### Q) What libraries did you add to the frontend? What are they used for?

### Q) What's the command to start the application locally?

(Default) `npm start`

---

# General:

### Q) If you had more time, what further improvements or new features would you add?

I'd include information on how to find the docs for the styleguide, or else possibly not include it. I don't have enough experience using a purely BEM kind of approach which this SCSS set up seems to encourage, so was a bit slowed down by this.

### Q) Which parts are you most proud of? And why?

My approach to integration testing, though it's admittedly controversial.
I prefer to focus on functional and integration testing in terms of a TDD approach - deciding on the information, and writing tests for the information that is expected for each action has, in my experience, been really helpful in architecting a solution, which I believe is at the heart of what test driven development is really about (rather than code coverage stats). This is a personal opinion only, and definitely open to argument/controversy

### Q) Which parts did you spend the most time with? What did you find most difficult?

Spent the most time on the layout, and ran out of time to finish it, spent too long trying to figure out if I could find info on the styleguide which in hindsight was time wasted.

### Q) How did you find the test overall? Did you have any issues or have difficulties completing?If you have any suggestions on how we can improve the test, we'd love to hear them.

The only problems I had in completing was in getting the layout done. I would have liked to add more tests to the components.
