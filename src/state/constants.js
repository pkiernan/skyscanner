const loadFlights = 'LOAD_FLIGHT_RESULTS';
export const LOAD_FLIGHT_RESULTS_CALL = `${loadFlights}_CALL`;
export const LOAD_FLIGHT_RESULTS_SUCCESS = `${loadFlights}_SUCCESS`;
export const LOAD_FLIGHT_RESULTS_FAIL = `${loadFlights}_FAIL`;
