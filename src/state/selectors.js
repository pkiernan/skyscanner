import { createSelector } from 'reselect';
import { find } from 'lodash';

const getState = ({ flights = {} }) => flights;

export const selectorIsLoaded = createSelector(
  getState,
  ({ isLoaded }) => isLoaded === true,
);
export const selectorItinerarires = createSelector(
  getState,
  ({ itineraries = [] }) => itineraries,
);
export const selectorLegs = createSelector(getState, ({ legs = [] }) => legs);

export const selectFlightData = createSelector(
  [selectorItinerarires, selectorLegs],
  (itinerariesList, legsList) => {
    if (itinerariesList.length && legsList.length) {
      return itinerariesList.map(({ legs = [], ...itinerary }) => ({
        ...itinerary,
        legs: legs.map(leg_id => {
          return find(legsList, { id: leg_id }, {});
        }),
      }));
    }
    return [];
  },
);
