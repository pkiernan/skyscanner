import {
  LOAD_FLIGHT_RESULTS_CALL,
  LOAD_FLIGHT_RESULTS_FAIL,
  LOAD_FLIGHT_RESULTS_SUCCESS,
} from './constants';

export const loadFlightsCall = payload => ({
  type: LOAD_FLIGHT_RESULTS_CALL,
  payload,
});
export const loadFlightsSuccess = payload => ({
  type: LOAD_FLIGHT_RESULTS_SUCCESS,
  payload,
});
export const loadFlightsFail = payload => ({
  type: LOAD_FLIGHT_RESULTS_FAIL,
  payload,
});
