import axios from 'axios';
import { takeLatest, put } from 'redux-saga/effects';
import { loadFlightsSuccess, loadFlightsFail } from './actions';
import { LOAD_FLIGHT_RESULTS_CALL } from './constants';

function* getFlights() {
  try {
    const response = yield axios({
      url: './flights.json',
      method: 'get',
      headers: {
        'content-type': 'application/json',
      },
    });
    yield put(loadFlightsSuccess(response));
  } catch (e) {
    yield put(loadFlightsFail(e));
  }
}

export function* watchFlights() {
  yield takeLatest(LOAD_FLIGHT_RESULTS_CALL, getFlights);
}
