import { combineReducers } from 'redux';
import { get } from 'lodash';
import {
  LOAD_FLIGHT_RESULTS_CALL,
  LOAD_FLIGHT_RESULTS_SUCCESS,
  LOAD_FLIGHT_RESULTS_FAIL,
} from '../state/constants';

const initState = {
  isLoaded: false,
  itineraries: [],
  legs: [],
};

const flightsReducer = (state = initState, action = {}) => {
  const { type, payload = {} } = action;
  switch (type) {
    case LOAD_FLIGHT_RESULTS_CALL:
      return {
        ...initState,
      };
    case LOAD_FLIGHT_RESULTS_SUCCESS:
      const legs = get(payload, 'data.legs', []);
      const itineraries = get(payload, 'data.itineraries', []);
      return {
        ...state,
        isLoaded: true,
        legs,
        itineraries,
      };
    case LOAD_FLIGHT_RESULTS_FAIL:
      return {
        ...state,
        isLoaded: true,
      };
    default:
      return state;
  }
};

export default combineReducers({
  flights: flightsReducer,
});
