import { runStateIntegrationTests } from '../utils/testing';
import * as selectors from './selectors';
import {
  loadFlightsCall,
  loadFlightsSuccess,
  loadFlightsFail,
} from './actions';
import {
  LOAD_FLIGHT_RESULTS_CALL,
  LOAD_FLIGHT_RESULTS_SUCCESS,
  LOAD_FLIGHT_RESULTS_FAIL,
} from './constants';
import {
  getSampleItineraries,
  getSampleLegs,
  getSampleFlightData,
} from '../utils/mocks';

/*
  Perform integration rather unit testing.
  In effect this means writing one test to test the action creators, the reducer, the selectors are correct
*/

const itineraries = getSampleItineraries();
const legs = getSampleLegs();
const flightData = getSampleFlightData();

const config = {
  id: 'flights',
  selectors,
  defaultValues: {
    isLoading: null,
    legs: [],
    intineraries: [],
  },
  actions: [
    {
      id: LOAD_FLIGHT_RESULTS_CALL,
      action: loadFlightsCall,
      expectedValues: {
        selectorIsLoaded: false,
        selectorItinerarires: [],
        selectorLegs: [],
        selectFlightData: [],
      },
    },
    {
      id: LOAD_FLIGHT_RESULTS_SUCCESS,
      action: loadFlightsSuccess,
      payload: {
        data: {
          itineraries,
          legs,
        },
      },
      expectedValues: {
        selectorIsLoaded: true,
        selectorItinerarires: itineraries,
        selectorLegs: legs,
        selectFlightData: flightData,
      },
    },
    {
      id: LOAD_FLIGHT_RESULTS_FAIL,
      action: loadFlightsFail,
      expectedValues: {
        selectorIsLoaded: true,
        selectorItinerarires: [],
        selectorLegs: [],
        selectFlightData: [],
      },
    },
  ],
};
runStateIntegrationTests(config);
