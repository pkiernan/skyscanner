import React, { Component } from 'react';
import moment from 'moment';
import { flightPropType } from './proptypes';

import STYLES from './styles.scss';
const getClassName = className => STYLES[className] || 'UNKNOWN';

class FlightResult extends Component {
  static propTypes = flightPropType.isRequired;
  formatTime(time) {
    return moment(time).format('HH:MM');
  }
  convertDuration(duration) {
    const hrs = Math.floor(duration / 60);
    const mins = duration % 60;
    return `${hrs}h ${mins}`;
  }
  renderAirlineLogo({ id, name }) {
    return (
      <div className={getClassName('flightResult__airlineLogo')}>
        <img
          src={`https://logos.skyscnr.com/images/airlines/favicon/${id}.png`}
          alt={name}
          title={name}
        />
      </div>
    );
  }
  renderLegs() {
    const { legs = [] } = this.props;
    return legs.map(leg => {
      const {
        id,
        departure_airport,
        arrival_airport,
        departure_time,
        arrival_time,
        stops,
        airline_name,
        airline_id,
        duration_mins,
      } = leg;
      return (
        <div key={id} className={getClassName('flightResult__legs')}>
          {this.renderAirlineLogo({ id: airline_id, name: airline_name })}
          <div className={getClassName('flightResult__timeport')}>
            <span className={getClassName('flightResult__timeport__time')}>
              {this.formatTime(departure_time)}
            </span>
            <span className={getClassName('flightResult__timeport__airport')}>
              {departure_airport}
            </span>
          </div>
          <div>-></div>
          <div className={getClassName('flightResult__timeport')}>
            <span className={getClassName('flightResult__timeport__time')}>
              {this.formatTime(arrival_time)}
            </span>
            <span className={getClassName('flightResult__timeport__airport')}>
              {arrival_airport}
            </span>
          </div>
          <div>
            <span className={getClassName('flightResult__duration')}>
              {this.convertDuration(duration_mins)}
            </span>
            <span
              className={getClassName(
                `flightResult__stops${stops === 0 ? '--direct' : ''}`,
              )}
            >
              {stops === 0 ? 'Direct' : `${stops} Stop(s)`}
            </span>
          </div>
        </div>
      );
    });
  }
  renderInfo() {
    const { price, agent } = this.props;
    return (
      <div className={getClassName('flightResult__info')}>
        <div className={getClassName('flightResult__info__price_agent')}>
          <span className={getClassName('flightResult__price')}>{price}</span>
          <span className={getClassName('flightResult__agent')}>{agent}</span>
        </div>
        <div>
          <button className={getClassName('flightResult__cta')}>Select</button>
        </div>
      </div>
    );
  }
  render() {
    return (
      <article className={getClassName('flightResult')}>
        {this.renderLegs()}
        {this.renderInfo()}
      </article>
    );
  }
}

export default FlightResult;
