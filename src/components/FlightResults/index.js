import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { selectorIsLoaded, selectFlightData } from '../../state/selectors';
import { flightPropType } from './proptypes';
import FlightResult from './FlightResult';
import { loadFlightsCall } from '../../state/actions';

import STYLES from './styles.scss';
const getClassName = className => STYLES[className] || 'UNKNOWN';

class FlightResults extends Component {
  // if I had more time I would abstract these through an adapter pattern so if the api changes, these props won't
  static propTypes = {
    isLoaded: PropTypes.bool.isRequired,
    loadFlights: PropTypes.func.isRequired,
    flightData: PropTypes.arrayOf(flightPropType).isRequired,
  };
  componentDidMount() {
    const { loadFlights } = this.props;
    loadFlights();
  }
  renderIsLoading() {
    const { isLoaded } = this.props;
    // replace with a loading wheel etc.
    return !isLoaded ? <span>loading...</span> : null;
  }
  renderContent() {
    const { flightData, isLoaded } = this.props;
    if (!isLoaded) return null;
    return flightData.length ? (
      flightData.map(flightData => (
        <FlightResult key={flightData.id} {...flightData} />
      ))
    ) : (
      <p>No results</p>
    );
  }
  render() {
    return (
      <section className={getClassName('flightResults')}>
        {this.renderIsLoading()}
        {this.renderContent()}
      </section>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  loadFlights: () => dispatch(loadFlightsCall()),
});

const mapStateToProps = state => ({
  isLoaded: selectorIsLoaded(state),
  flightData: selectFlightData(state),
});

export default connect(mapStateToProps, mapDispatchToProps)(FlightResults);
