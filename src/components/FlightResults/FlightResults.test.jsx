import { runComponentIntegrationTests } from '../../utils/testing';
import FlightResults from './index';
import {
  loadFlightsCall,
  loadFlightsSuccess,
  loadFlightsFail,
} from '../../state/actions';
import {
  LOAD_FLIGHT_RESULTS_CALL,
  LOAD_FLIGHT_RESULTS_SUCCESS,
  LOAD_FLIGHT_RESULTS_FAIL,
} from '../../state/constants';
import {
  getSampleItineraries,
  getSampleLegs,
  getSampleFlightData,
} from '../../utils/mocks';

/*
  Perform integration rather unit testing.
  In effect this means writing one test to test the action creators, the reducer, the selector and the component props are
  correct per action dispatched.
*/

const itineraries = getSampleItineraries();
const legs = getSampleLegs();
const flightData = getSampleFlightData();

const config = {
  id: 'FlightResults',
  Component: FlightResults,
  actions: [
    {
      id: LOAD_FLIGHT_RESULTS_CALL,
      action: loadFlightsCall,
      expectedProps: {
        isLoaded: false,
        flightData: [],
      },
    },
    {
      id: LOAD_FLIGHT_RESULTS_SUCCESS,
      action: loadFlightsSuccess,
      payload: {
        data: {
          itineraries,
          legs,
        },
      },
      expectedProps: {
        isLoaded: true,
        flightData,
      },
    },
    {
      id: LOAD_FLIGHT_RESULTS_FAIL,
      action: loadFlightsFail,
      expectedProps: {
        isLoaded: true,
        flightData: [],
      },
    },
  ],
};
runComponentIntegrationTests(config);
