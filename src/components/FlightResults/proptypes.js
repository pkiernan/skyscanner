import PropTypes from 'prop-types';

export const legPropType = PropTypes.shape({
  id: PropTypes.string.isRequired,
  departure_airport: PropTypes.string.isRequired,
  arrival_airport: PropTypes.string.isRequired,
  departure_time: PropTypes.string.isRequired,
  arrival_time: PropTypes.string.isRequired,
  stops: PropTypes.number.isRequired,
  airline_name: PropTypes.string.isRequired,
  airline_id: PropTypes.string.isRequired,
  duration_mins: PropTypes.number.isRequired,
});

export const flightPropType = PropTypes.shape({
  id: PropTypes.string.isRequired,
  price: PropTypes.string.isRequired,
  agent: PropTypes.string.isRequired,
  legs: PropTypes.arrayOf(legPropType),
});
