import React from 'react';
import FlightResults from '../FlightResults';

import Header from '../Header';

import STYLES from './App.scss';

const getClassName = className => STYLES[className] || 'UNKNOWN';

const App = () => (
  <div className={getClassName('App')}>
    <Header />
    <main className={getClassName('App__main')}>
      <FlightResults />
    </main>
  </div>
);

export default App;
