import { all, fork } from 'redux-saga/effects';
import { watchFlights } from './state/services';

export default function* root() {
  yield all([fork(watchFlights)]);
}
